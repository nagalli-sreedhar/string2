function getMonth(date) {
    const result =  date.split('/');
    return result[1];
}

module.exports = getMonth;