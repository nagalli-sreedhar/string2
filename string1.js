function getNumericFormat(number) {

    let res = number.replace(/[-,$]/g, '');

    if(isNaN(res))
    {
        return 0;
    }
    else
    {
        return Number(res);
    }
}
module.exports = getNumericFormat;