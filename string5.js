    function getString(givenArray) {
        if(givenArray.length==0)
        {
            return "";
        }
        return givenArray.reduce((str, item) => str+item+" " , "");
    }
    
 module.exports = getString;